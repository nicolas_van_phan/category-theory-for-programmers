
This repository contains some notes and Coq proofs on the matters discussed in Bartosz Milewski's notes : Category Theory For Programmers.

Link here : [https://bartoszmilewski.com/2014/10/28/category-theory-for-programmers-the-preface].

The `.v` files should be named the same as the book chapters.

## Usage

Run `make` to compile the files.

Run `make html` to generate the HTML version of the Coq files.
