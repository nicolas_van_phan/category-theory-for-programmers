(** * Functors *)

(* Set Warnings "-notation-overridden,-parsing,-deprecated-hint-without-locality". *)
From Coq Require Import Logic.
From Coq Require Import Logic.FunctionalExtensionality.

(** Notes from Bartosz Milewski's Category Theory for Programmers.

    See {https://bartoszmilewski.com/2015/01/20/functors}. *)

(** Functors are a mapping between categories
    - It maps objects to objects
    - It maps morphisms to morphisms

    And the mapping preserves the category structure, which means :
    - preservation of composition : [F (f.g) = F f . F g]
    - preservation of identity    : [F (id_src) = id_dest] *)

(** Here, we'll consider the category where objects are elements of [Type] *)

Definition id (T : Type) : T -> T :=
  fun x => x.

(* ################################################################# *)
(** * Functors in programming *)

(* ================================================================= *)
(** ** The Option Functor *)

(** *** Definition *)

Inductive option (T : Type) : Type :=
| Some (t : T)
| None
.
Arguments Some {T}.
Arguments None {T}.

Definition option_map {T1 T2 : Type} (f : T1 -> T2) : option T1 -> option T2 :=
  fun t1_opt =>
    match t1_opt with
    | Some v => Some (f v)
    | None => None
    end.

(** *** Proof that it's a functor *)

(** 1. Preservation of identity : F id = id *)

Theorem option_id : forall (T : Type), option_map (id T) = id (option T).
Proof.
  intros.
  apply functional_extensionality.
  intros.
  unfold id.
  destruct x; reflexivity.
Qed.

Definition comp {T1 T2 T3 : Type} (f1 : T1 -> T2) (f2 : T2 -> T3) : T1 -> T3 :=
  fun x => f2 (f1 x).

(** 2. Preservation of composition : F (f.g) = F f . F g *)

Theorem option_comp : forall (T1 T2 T3 : Type) (f : T1 -> T2) (g : T2 -> T3),
  option_map (comp f g) = comp (option_map f) (option_map g).
Proof.
  intros. 
  apply functional_extensionality.
  intros.
  destruct x; unfold comp; reflexivity.
Qed.

(* ================================================================= *)
(** ** Lists *)

(** *** Definition *)

Inductive list (T : Type) :=
| Nil
| Cons (hd : T) (tl : list T)
.

Arguments Nil {T}.
Arguments Cons {T}.

Fixpoint list_map {T1 T2 : Type} (f : T1 -> T2) (l : list T1) : list T2 :=
  match l with
  | Nil => Nil
  | Cons hd tl => Cons (f hd) (list_map f tl)
  end.

(** *** Proof that it's a functor *)

(** 1. Preservation of identity *)

Theorem list_id : forall (T : Type), list_map (id T) = id (list T).
Proof.
  intros.  apply functional_extensionality.  intros.
  unfold id.
  induction x.
  -  reflexivity.
  -  simpl. rewrite IHx. reflexivity.
Qed.

(** 2. Preservation of composition : F (f.g) = F f . F g *)

Theorem list_comp : forall (T1 T2 T3 : Type) (f : T1 -> T2) (g : T2 -> T3),
  list_map (comp f g) = comp (list_map f) (list_map g).
Proof.
  intros.
  apply functional_extensionality.
  intros.
  induction x.
  - simpl. unfold comp. simpl. reflexivity.  
  - simpl. rewrite IHx. unfold comp. simpl. reflexivity.
Qed.

(* ================================================================= *)
(** ** The Reader functor *)

(** *** Definition *)

Definition arrow (A B : Type) := A -> B.
Definition narrow := arrow nat. (* B -> (nat -> B) *)

Definition narrow_map {T1 T2 : Type} (f : T1 -> T2) (x : narrow T1) : narrow T2 :=
  let t1_of_nat : nat -> T1 := x in
  let t2_of_t1  : T1  -> T2 := f in
  let t2_of_nat : nat -> T2 := comp x f in
  let narrow_t2 : narrow T2 := t2_of_nat in
  narrow_t2.

(* Or more tersely *)
Definition narrow_map' {T2 T3 : Type} := comp (T1 := nat) (T2 := T2) (T3 := T3).

(** *** Preservation of indentity *)

Theorem narrow_id :
  forall (T : Type), narrow_map (id T) = id (narrow T).
Proof.
  intros.
  apply functional_extensionality.
  intros.
  unfold id.
  unfold narrow_map.
  unfold comp.
  reflexivity.
Qed.

(** *** Preservation of composition *)

Theorem narrow_comp : forall (T1 T2 T3 : Type) (f : T1 -> T2) (g : T2 -> T3),
  narrow_map (comp f g) = comp (narrow_map f) (narrow_map g).
Proof.
  intros.  apply functional_extensionality.  intros.
  unfold comp.
  unfold narrow_map.
  unfold comp.
  reflexivity.
Qed.

(** More generally, the two rules for being a functor are : *)

Definition object_map := Type -> Type.
Definition morphism_map (omap : object_map) :=
  forall T U, (T -> U) -> omap T -> omap U.


(** *** Preservation of indentity *)

Definition preserve_id
  (omap   : object_map)
  (mmap   : morphism_map omap) :=
  forall
    (T : Type),
  mmap T T (id T) = id (omap T).

(** *** Preservation of composition *)

Definition preserve_composition
  (omap   : object_map)
  (mmap   : morphism_map omap) :=
  forall
    (T1 T2 T3 : Type)
    (f : T1 -> T2)
    (g : T2 -> T3),
  mmap T1 T3 (comp f g) = comp (mmap T1 T2 f) (mmap T2 T3 g).

Definition is_functor
  (omap   : object_map)
  (mmap   : morphism_map omap) :=
  preserve_id omap mmap /\ preserve_composition omap mmap.

(** ** Functors as Containers *)

(**

  Quote :

    So I like to think of the functor object (an object of the type generated by an endofunctor)

    as containing a value or values of the type over which it is parameterized,

    even if these values are not physically present there
  
  Example : The list functor
  This functor takes [t : Type] and returns [list t : Type],

  so it maps from [Type] to [Type], so it's an endofunctor.

  The "functor object" is the type generated by [list], it's [list t].

  It is parameterized by [t].

  So, a way to rephrase the quote, with the [list] example, is :

  So I like to think of [list t] as containing values of type [t].
  
*)

(* data Maybe a = Nothing | Just a *)
(*
Inductive option (T : Type) : Type :=
| Some (t : T)
| None . *)


(* data List a = Nil | Cons a (List a) *)
(*
Inductive list (T : Type) :=
| Nil
| Cons (hd : T) (tl : list T) . *)


(* data Const c a = Const c *)

Inductive const (C A : Type) : Type :=
| Const (c : C)
.
Arguments Const {C A}.

Definition const_map (C : Type) [T1 T2 : Type] (f : T1 -> T2) (x : const C T1) : const C T2 :=
  match x with
  | Const c => Const c
  end.

Theorem const_is_functor : forall C : Type,
  is_functor (const C) (const_map C).
Proof.
  intros.
  unfold is_functor.
  split.
  (* id *)
  - unfold preserve_id. intros. apply functional_extensionality. intros. 
    unfold const_map.
    destruct x.
    unfold id.
    reflexivity.
  (* comp *)
  - unfold preserve_composition.  intros. apply functional_extensionality. intros.
    unfold comp.
    unfold const_map. destruct x.
    reflexivity.
Qed.

(** ** Functor Composition *)

(**
  Quoting from the book :
    It's not hard to convince yourself that functors between categories compose,
    just like functions between sets compose...
  
  Here we prove that functors compose. *)

Definition mmap_comp {omap1 omap2 : object_map}
  (mmap1 : morphism_map omap1)
  (mmap2 : morphism_map omap2) :=
  fun T U => comp (mmap1 T U) (mmap2 (omap1 T) (omap1 U)).

Ltac unfold_is_functor H :=
  unfold is_functor in H
; destruct H as [Hi Hc]
; unfold preserve_id in Hi
; unfold preserve_composition in Hc
; unfold comp in Hc.

Theorem functors_compose :
  forall
  (omap1 : object_map)
  (mmap1 : morphism_map omap1)
  (omap2 : object_map)
  (mmap2 : morphism_map omap2),
  is_functor omap1 mmap1 ->
  is_functor omap2 mmap2 ->
  is_functor (comp omap1 omap2) (mmap_comp mmap1 mmap2).
Proof.
  intros omap1 mmap1 omap2 mmap2 H1 H2.
  unfold_is_functor H1. rename Hi into Hi1.  rename Hc into Hc1.
  unfold_is_functor H2. rename Hi into Hi2.  rename Hc into Hc2.
  split.
  - (* preserve id *)
    unfold preserve_id. intros.
    unfold mmap_comp.
    unfold comp.
    rewrite (Hi1 _).
    rewrite (Hi2 _).
    reflexivity.
  - (* preserve comp *)
    unfold preserve_composition. intros.
    unfold mmap_comp.
    unfold comp.
    rewrite (Hc1 _ _ _ _ _).
    rewrite (Hc2 _ _ _ _ _).
    reflexivity.
Qed.

