
build: 
	coqc functors.v

html: build
	coqdoc functors.v --no-index -o html/functors.html

clean:
	rm -f functors.vo
	rm -f functors.vok
	rm -f functors.vos
	rm -f functors.glob
	rm -f .functors.aux